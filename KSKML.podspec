Pod::Spec.new do |s|
s.name             = "KSKML"
s.version          = "1.0"
s.summary          = "Moduł KtoMaLek do VisiMed i KML."
s.homepage         = "https://kamsoft.pl"
s.license          = 'Propertiary.'
s.author           = { "Mateusz Popiało" => "mpopialo@kamsoft.pl" }
s.source           = { :git => "https://gitlab.com/FruitAddict/KMLTest.git", :tag => s.version }

s.platform     = :ios, '9.0'
s.requires_arc = true

s.source_files = 'KSKML/Classes'
s.resources = 'KSKML/Assets/*'

s.framework = 'UIKit'
s.dependency 'Alamofire'
s.module_name = 'KSKML'
end
